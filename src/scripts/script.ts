import {InitialParams, MenuItem} from "./types";
import '@fortawesome/fontawesome-free/js/fontawesome'
import '@fortawesome/fontawesome-free/js/solid'
import '@fortawesome/fontawesome-free/js/regular'
import '@fortawesome/fontawesome-free/js/brands'
import '../styles/index.scss';

export function initialMenu(params: InitialParams) {
  const {settings, list} = params;
  const menuWrap = document.getElementById(settings.wrapper);
  if (menuWrap) {
    menuWrap.setAttribute('data-menu', 'radial');
    menuWrap.appendChild(addButtonSwitch());
    getList(list).map(item => menuWrap.appendChild(item));
  }
}

function addButtonSwitch() {
  const buttonTag = document.createElement('button');
  buttonTag.setAttribute('id', 'switch_radial_menu');
  return buttonTag;
}

function getList(list: MenuItem[]) {
  const deg = 360 / list.length;
  return list.map((item, index,) => createItemMenu(item, deg, index));
}

function createItemMenu(item: MenuItem, deg: number, index: number) {
  const {label, icon, url} = item;

  const liTag = document.createElement('li');
  const linkTag = document.createElement('a');
  const iconTag = document.createElement('i');

  liTag.setAttribute('data-animation', `transform: rotate(${(index * deg) - 45}deg) skewY(0)`);
  liTag.setAttribute('class', `radial_menu_item`);
  linkTag.setAttribute('href', url);
  linkTag.setAttribute('title', label);
  linkTag.setAttribute('style', `transform: rotate(${(360 - (index * deg)) + 45}deg)`);
  iconTag.setAttribute('class', `${icon} icon`);

  linkTag.appendChild(iconTag);
  liTag.appendChild(linkTag);

  return liTag;
}

document.onclick = function (event) {
  const et = (<HTMLElement>event.target);
  if (et.id === 'switch_radial_menu') {
    toggleAnimation(et);
  }
}

function toggleAnimation(et: HTMLElement) {
  if (et.parentElement) {
    et.parentElement.classList.toggle('active');
    const list = document.getElementsByClassName('radial_menu_item');
    if (et.parentElement.classList.contains('active')) {
      for (let i = 0; i < list.length; i++) {
        list[i].setAttribute('style', `${list[i].getAttribute('data-animation')}`);
      }
    } else if (!et.parentElement.classList.contains('active')) {
      for (let i = 0; i < list.length; i++) {
        list[i].setAttribute('data-animation', `${list[i].getAttribute('style')}`);
        list[i].removeAttribute('style');
      }
    }
  }
}