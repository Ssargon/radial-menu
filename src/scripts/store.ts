export default {
  settings: {
    wrapper: 'circle'
  },
  list: [
    {
      label: 'Google',
      icon: 'fab fa-google',
      url: 'google.com',
    },

    {
      label: 'Play Market',
      icon:'fab fa-google-play',
      url: 'play.google.com',
    },
    {
      label: 'Angular',
      icon:'fab fa-angular',
      url: 'angular.io',
    },
    {
      label: 'React',
      icon:'fab fa-react',
      url: 'reactjs.org',
    },
    {
      label: 'Vue',
      icon:'fab fa-vuejs',
      url: 'vuejs.org',
    },
    {
      label: 'Electron',
      icon:'fas fa-bolt',
      url: 'electronjs.org',
    },
  ]
}