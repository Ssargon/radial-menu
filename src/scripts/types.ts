export interface InitialParams {
  list: MenuItem[];
  settings: Settings;
}

export interface MenuItem {
  label: string,
  icon: string,
  url: string,
}

export interface Settings {
  wrapper: string;
}